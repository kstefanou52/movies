//
//  Extensions+Foundation.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

extension URL {
    func appendingQueryParameters(_ parameters: [String: Any]) -> URL {
        var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true)
        var items = urlComponents?.queryItems ?? []
        items += parameters.map({ URLQueryItem(name: $0, value: "\($1)") })
        urlComponents?.queryItems = items
        
        return urlComponents?.url ?? self
    }
}
