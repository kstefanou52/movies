//
//  Extensions+UIKit.swift
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func showLoader(_ style: UIActivityIndicatorView.Style = .gray) {
        OperationQueue.main.addOperation {
            let loader = UIActivityIndicatorView(style: style)
            loader.tag = 52
            
            guard self.subviews.filter({ $0 is UIActivityIndicatorView && $0.tag == 52 }).isEmpty else { return } //check if loader is here already
            
            loader.center = self.center
            self.addSubview(loader)
            loader.startAnimating()
        }
    }
    
    func hideLoader() {
        OperationQueue.main.addOperation {
            let loader = self.subviews.filter({ $0 is UIActivityIndicatorView && $0.tag == 52 }).first
            loader?.removeFromSuperview()
        }
    }
}

extension UIViewController {
    @objc func informUser(_ message: String?, appearCompletion: @escaping() -> Void = { }, dismissCompletion: @escaping() -> Void = { } ) {
        let controller: InformViewController = InformViewController(message: message)
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle  = .crossDissolve
        controller.dismissCompletionBlock = dismissCompletion
        
        OperationQueue.main.addOperation { self.present(controller, animated: true, completion: appearCompletion) }
    }
}

extension UIImageView {
    @objc func getImage(from path: String?) {
        if let data = ImageCacher.shared.getCachedImage(for: path) { image = UIImage(data: data) ; return }
        showLoader()
        HTTPClient.shared.getImage(path: path) { [weak self] (data) in
            self?.hideLoader()
            guard let data = data else {
                OperationQueue.main.addOperation { self?.image = UIImage(named: "empty_image") }
                return
            }
            
            ImageCacher.shared.cacheImage(path: path, data: data)
            OperationQueue.main.addOperation { self?.image = UIImage(data: data) }
        }
    }
}
