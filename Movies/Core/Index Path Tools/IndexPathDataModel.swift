//
//  IndexPathDataModel.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc class IndexPathDataModel: NSObject {
    
    @objc private(set) var items: [IndexPathItem]
    
    @objc init(items: [IndexPathItem]) {
        self.items = items
    }
}

// MARK: Helpers
extension IndexPathDataModel {
    @objc func numberOfItems() -> Int {
        return items.count
    }
    
    @objc func item(for indexPath: IndexPath) -> IndexPathItem? {
        guard indexPath.row < items.count else { return nil }
        return items[indexPath.row]
    }
    
    func deleteAll() {
        items.removeAll()
    }
    
    func getAllIndexPaths() -> [IndexPath] {
        var indexPaths: [IndexPath] = []
        for index in 0..<items.count {
            indexPaths.append(IndexPath(row: index, section: 0))
        }
        
        return indexPaths
    }
    
}
