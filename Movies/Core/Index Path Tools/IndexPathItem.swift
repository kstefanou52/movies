//
//  IndexPathItem.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc class IndexPathItem: NSObject {
    @objc let identifier: String
    @objc let cellIdentifier: String
    @objc var data: Any?
    
    @objc init(identifier: String, cellIdentifier: String, data: Any?) {
        self.identifier = identifier
        self.cellIdentifier = cellIdentifier
        self.data = data
    }
}
