//
//  InformViewController.swift
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import UIKit

class InformViewController: UIViewController {

    var dismissCompletionBlock: (() -> Void)?
    private var informMessage: String = "An error occured, please try again"
    
    //Outlets
    @IBOutlet private weak var informLabel: UILabel!
    
    init(message: String?) {
        super.init(nibName: "InformViewController", bundle: nil)
        
        if let message = message { self.informMessage = message }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    private func setUp() {
        informLabel.text = informMessage
    }
    
    //Outlet Actions
    @IBAction private func dismissButtonPressed(_ sender: UIButton) {
        UIView.animate(withDuration: 0.4, animations: {
            self.view.alpha = 0
        }) { (_) in
            self.dismiss(animated: false, completion: self.dismissCompletionBlock)
        }
    }
}
