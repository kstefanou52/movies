//
//  Error.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

// https://www.themoviedb.org/documentation/api/status-codes
@objc enum HTTPClientError: Int, Error {
    case none
    case invalidResponse
    case invalidRequest
    case parsingError
    case AUTH_FAILED = 401
    case FAILED = 500
    case SERVICE_UNAVAILABLE = 501
}
