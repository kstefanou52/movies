//
//  InterfaceRouter.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation
import UIKit

class InterfaceRouter {
    
    private static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    static func instantiate<T>(_ class: T.Type) -> T {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
    }
}
