//
//  ImageCacher.swift
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation
import UIKit

class ImageCacher {

    static let shared = ImageCacher()

    let imageCache = NSCache<AnyObject, AnyObject>()
    
    func cacheImage(path: String?, data: Data) {
        guard let path = path else { return }
        OperationQueue.main.addOperation {
            self.imageCache.setObject(data as AnyObject, forKey: path as AnyObject)
        }
    }
    
    func getCachedImage(for path: String?) -> Data? {
        guard let path = path else { return nil }
        return imageCache.object(forKey: path as AnyObject) as? Data
    }
}
