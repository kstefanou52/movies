//
//  PaginationModel.swift
//  Movies
//
//  Created by kostis stefanou on 7/20/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

class PaginationModel {
    
    let totalPages: Int
    let currentPage: Int
    let searchQuery: String
    var isLoading: Bool = false
    
    var hasNextPage: Bool {
        return currentPage < totalPages
    }

    init(totalPages: Int, currentPage: Int, searchQuery: String) {
        self.totalPages = totalPages
        self.currentPage = currentPage
        self.searchQuery = searchQuery
    }
}
