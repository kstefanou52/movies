//
//  SearhEmptyTableViewCell.swift
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import UIKit

class SearhEmptyTableViewCell: UITableViewCell {

    static let identifier: String = "SearhEmptyTableViewCell"
    
    //Outlets
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = "No results found"
    }
}
