//
//  SearchPaginationLoadingTableViewCell.swift
//  Movies
//
//  Created by kostis stefanou on 7/20/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import UIKit

class SearchPaginationLoadingTableViewCell: UITableViewCell {

    static let identifier: String = "SearchPaginationLoadingTableViewCell"
    
    //Outlets
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicator.startAnimating()
    }
}
