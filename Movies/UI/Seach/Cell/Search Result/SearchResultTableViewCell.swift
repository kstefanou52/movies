//
//  SearchResultTableViewCell.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {
    
    static let identifier: String = "SearchResultTableViewCell"
    
    //Outlets
    @IBOutlet private weak var mainImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var averageVoteLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    func setup(with data: APIResponseSearchResult) {
        titleLabel.text = data.name ?? "-"
        averageVoteLabel.text = String(data.voteAverage ?? 0)
        dateLabel.text = data.releaseDate ?? data.firstAirDate ?? "-"
        
        mainImageView.getImage(from: data.posterPath)
        
        let showDisclosureIndicator: Bool = data.mediaType != nil && data.id != nil
        accessoryType = showDisclosureIndicator ? .disclosureIndicator : .none
    }
    
}
