//
//  SearchViewController.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    private var dataModel = IndexPathDataModel(items: [])
    
    //Outlets
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

    private func setUp() {
        title = "Movies"
        tableView.register(UINib(nibName: SearchResultTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: SearchResultTableViewCell.identifier)
        tableView.register(UINib(nibName: SearhEmptyTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: SearhEmptyTableViewCell.identifier)
        tableView.register(UINib(nibName: SearchPaginationLoadingTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: SearchPaginationLoadingTableViewCell.identifier)
    }
    
    private func getSearchResults(query: String, page: Int, showLoader: Bool = false) {
        if showLoader { view.showLoader() }
        HTTPClient.shared.getSearchResults(with: query, page: page) { (response, error) in
            self.view.hideLoader()
            guard let response = response, error == .none else { self.informUser("an error occured, please try again later") ; return }
            self.createDataModel(with: response, query: query)
        }
    }
    
    private func createDataModel(with data: APIResponseSearch, query: String) {
        var items: [IndexPathItem] = dataModel.items
        
        if items.last?.cellIdentifier == SearchPaginationLoadingTableViewCell.identifier {
            items.removeLast()
        }
        
        for result in data.results ?? [] {
            let newItem = IndexPathItem(identifier: String(result.id ?? 0), cellIdentifier: SearchResultTableViewCell.identifier, data: result)
            items.append(newItem)
        }
        
        if (data.page ?? 0) < (data.totalPages ?? 0) {
            let paginationData = PaginationModel(totalPages: data.totalPages ?? 0, currentPage: data.page ?? 0, searchQuery: query)
            let paginationItem = IndexPathItem(identifier: "pagination", cellIdentifier: SearchPaginationLoadingTableViewCell.identifier, data: paginationData)
            items.append(paginationItem)
        }
        
        if items.isEmpty {
            let emptyItem = IndexPathItem(identifier: "empty", cellIdentifier: SearhEmptyTableViewCell.identifier, data: "")
            items.append(emptyItem)
        }
        
        dataModel = IndexPathDataModel(items: items)
        OperationQueue.main.addOperation { self.tableView.reloadData() }
    }
    
    private func pushDetailsViewController(for mediaType: MediaType, with id: Int) {
        let detailsViewController = InterfaceRouter.instantiate(DetailsViewController.self)
        detailsViewController.mediaId = id
        detailsViewController.mediaTypeId = mediaType.rawValue
        navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

// MARK: UITableView
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let indexPathItem = dataModel.item(for: indexPath), let data = indexPathItem.data else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: indexPathItem.cellIdentifier, for: indexPath)
        
        switch (cell, data) {
        case (let cell as SearchResultTableViewCell, let data as APIResponseSearchResult):
            cell.setup(with: data)
            
        default: break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let indexPathItem = dataModel.item(for: indexPath), let data = indexPathItem.data as? APIResponseSearchResult else { return }
        guard let mediaType = data.mediaType, let id = data.id else { return }
        pushDetailsViewController(for: mediaType, with: id)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let indexPathItem = dataModel.item(for: indexPath), let data = indexPathItem.data as? PaginationModel, indexPathItem.cellIdentifier == SearchPaginationLoadingTableViewCell.identifier else { return }
        if data.isLoading { return }
        data.isLoading = true
        getSearchResults(query: data.searchQuery, page: data.currentPage + 1)
    }
}

// MARK: UISearchBar
extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        guard let query = searchBar.text, query.count >= 1 else { informUser("Please enter at least one character in order to perform search") ; return }
        
        let allIndexPaths = dataModel.getAllIndexPaths()
        dataModel.deleteAll()
        tableView.deleteRows(at: allIndexPaths, with: .top)
        getSearchResults(query: query, page: 1, showLoader: true)
    }
}
