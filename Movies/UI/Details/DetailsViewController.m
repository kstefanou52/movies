//
//  DetailsViewController.m
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//


#import "DetailsViewController.h"
#import "Movies-Swift.h"
#import "DetailsInfoTableViewCell.h"
#import "DetailsTrailerTableViewCell.h"

@interface DetailsViewController () {
    IndexPathDataModel *dataModel;
}

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
}

- (void)setUp {
    [tableView registerNib:[UINib nibWithNibName:@"DetailsInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"DetailsInfoTableViewCell"];
    [tableView registerNib:[UINib nibWithNibName:@"DetailsTrailerTableViewCell" bundle:nil] forCellReuseIdentifier:@"DetailsTrailerTableViewCell"];
    
    dataModel = [[IndexPathDataModel alloc] initWithItems:@[]];
    MediaType mediaType = (MediaType) self.mediaTypeId;
    
    switch (mediaType) {
        case MediaTypeMovie:
            self.title = @"Movie Details";
            [self getMovieDetails];
            break;
            
        case MediaTypeTvShow:
            self.title = @"TV Show Details";
            [self getTVShowDetails];
            break;
    }
}

#pragma mark - Networking

- (void)getMovieDetails {
    [HTTPClient.shared getMovieDetailsFor:self.mediaId completion:^(APIResponseMovieDetails * _Nullable response, HTTPClientError error) {
        if (response == nil || error != HTTPClientErrorNone) {
            [self informUser:@"An error occured, please try again later" appearCompletion:^{ } dismissCompletion:^{
                [[self navigationController] popViewControllerAnimated:true];
            }];
            return ;
        };
        
        [self createDataModelForMovie:response];
    }];
}

- (void)getTVShowDetails {
    [HTTPClient.shared getTVShowDetailsFor:self.mediaId completion:^(APIResponseTVShowDetails * _Nullable response, HTTPClientError error) {
        if (response == nil || error != HTTPClientErrorNone) {
            [self informUser:@"An error occured, please try again later" appearCompletion:^{ } dismissCompletion:^{
                [[self navigationController] popViewControllerAnimated:true];
            }];
            return ;
        };
        [self createDataModelForTVShow:response];
    }];
}

#pragma mark - DataSource

- (void)createDataModelForMovie:(APIResponseMovieDetails *)data {
    NSMutableArray<IndexPathItem *> *items = [NSMutableArray new];
    
    IndexPathItem *newItem = [[IndexPathItem alloc] initWithIdentifier:@"movie" cellIdentifier:@"DetailsInfoTableViewCell" data:data];
    [items addObject:newItem];

    [items addObjectsFromArray:[self createVideoIndexPathItems:data.videos]];
    
    dataModel = [[IndexPathDataModel alloc] initWithItems:items];
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [self->tableView reloadData]; }];
}

- (void)createDataModelForTVShow:(APIResponseTVShowDetails *)data {
    NSMutableArray<IndexPathItem *> *items = [NSMutableArray new];
    
    IndexPathItem *newItem = [[IndexPathItem alloc] initWithIdentifier:@"tvShow" cellIdentifier:@"DetailsInfoTableViewCell" data:data];
    [items addObject:newItem];
    
    [items addObjectsFromArray:[self createVideoIndexPathItems:data.videos]];
    
    dataModel = [[IndexPathDataModel alloc] initWithItems:items];
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [self->tableView reloadData]; }];
}

- (NSMutableArray *)createVideoIndexPathItems:(APIResponseVideo *)data {
    if (data.results.count == 0) { return [NSMutableArray new]; }
    NSMutableArray<IndexPathItem *> *items = [NSMutableArray new];

    for (APIResponseVideoResult *item in data.results) {
        if (item.videoType != VideoTypeTrailer) { continue ; }
        IndexPathItem *newTrailerItem = [[IndexPathItem alloc] initWithIdentifier:[NSString stringWithFormat:@"trailer %@", item.key] cellIdentifier:@"DetailsTrailerTableViewCell" data:item];
        [items addObject:newTrailerItem];
    }

    return items;
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataModel numberOfItems];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    IndexPathItem *indexPathItem = [dataModel itemFor:indexPath];
    if (indexPathItem == nil || indexPathItem.data == nil) { return [[UITableViewCell alloc] init ]; }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indexPathItem.cellIdentifier forIndexPath:indexPath];
    
    if ([cell isKindOfClass:DetailsInfoTableViewCell.class]) {
        DetailsInfoTableViewCell *detailsCell = cell;
        
        if ([indexPathItem.data isKindOfClass:APIResponseMovieDetails.class]) {
            [detailsCell setupForMovie:indexPathItem.data];
        } else if ([indexPathItem.data isKindOfClass:APIResponseTVShowDetails.class]) {
            [detailsCell setupForTVShow:indexPathItem.data];
        };
    };
    
    if ([cell isKindOfClass:DetailsTrailerTableViewCell.class]) {
        DetailsTrailerTableViewCell *trailerCell = cell;
        
        if ([indexPathItem.data isKindOfClass:APIResponseVideoResult.class]) {
            [trailerCell setup:indexPathItem.data];
        }
    };
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    IndexPathItem *indexPathItem = [dataModel itemFor:indexPath];
    if (indexPathItem == nil || indexPathItem.data == nil) { return ; }
    
    if ([indexPathItem.data isKindOfClass:APIResponseVideoResult.class]) {
        APIResponseVideoResult *videoResult = indexPathItem.data;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:videoResult.videoURL] options: @{} completionHandler: nil];
    }
}

@end
