//
//  DetailsInfoTableViewCell.h
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movies-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailsInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;

- (void)setupForMovie:(APIResponseMovieDetails*) data;
- (void)setupForTVShow:(APIResponseTVShowDetails*) data;

@end

NS_ASSUME_NONNULL_END
