//
//  DetailsInfoTableViewCell.m
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//

#import "DetailsInfoTableViewCell.h"
#import "Movies-Swift.h"

@implementation DetailsInfoTableViewCell

- (void)setupForTVShow:(APIResponseTVShowDetails*) data {
    [_mainImageView getImageFrom:data.backdropPath];
    
    if (data.name != nil) {
        [_titleLabel setText:data.name];
    }
    
    if (data.overview != nil) {
        [_summaryLabel setText:data.overview];
    }
    
    if (data.genres.firstObject.name != nil) {
        [_genreLabel setText:[NSString stringWithFormat:@"Genre %@", data.genres.firstObject.name]];
    } else {
        [_genreLabel setText:@""];
    }
}

- (void)setupForMovie:(APIResponseMovieDetails*) data {
    [_mainImageView getImageFrom:data.backdropPath];
    
    if (data.title != nil) {
        [_titleLabel setText:data.title];
    }
    
    if (data.overview != nil) {
        [_summaryLabel setText:data.overview];
    }
    
    if (data.genres.firstObject.name != nil) {
        [_genreLabel setText:[NSString stringWithFormat:@"Genre %@", data.genres.firstObject.name]];
    } else {
        [_genreLabel setText:@""];
    }
}

@end
