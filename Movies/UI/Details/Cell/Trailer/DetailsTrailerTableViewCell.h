//
//  DetailsTrailerTableViewCell.h
//  Movies
//
//  Created by kostis stefanou on 7/19/19.
//  Copyright © 2019 silonk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movies-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailsTrailerTableViewCell : UITableViewCell

- (void)setup:(APIResponseVideoResult*) data;

@end

NS_ASSUME_NONNULL_END
