//
//  DetailsTrailerTableViewCell.m
//  Movies
//
//  Created by kostis stefanou on 7/19/19.
//  Copyright © 2019 silonk. All rights reserved.
//

#import "DetailsTrailerTableViewCell.h"
#import "Movies-Swift.h"

@implementation DetailsTrailerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textLabel.numberOfLines = 0;
}

- (void)setup:(APIResponseVideoResult*) data; {
    switch (data.videoProvider) {
        case VideoProviderYoutube:
            [self.textLabel setText:[NSString stringWithFormat:@"watch %@ on youtube", data.name]];
            break;
        case VideoProviderVimeo:
            [self.textLabel setText:[NSString stringWithFormat:@"watch %@ on vimeo", data.name]];
            break;
        case VideoProviderOther:
            [self.textLabel setText:[NSString stringWithFormat:@"watch %@", data.name]];
            break;
    }
}

@end
