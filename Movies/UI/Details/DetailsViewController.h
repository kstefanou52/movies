//
//  DetailsViewController.h
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    __weak IBOutlet UITableView *tableView;
}

@property (nonatomic) NSInteger mediaId;
@property (nonatomic) NSInteger mediaTypeId;

@end

NS_ASSUME_NONNULL_END
