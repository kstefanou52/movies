//
//  HTTPEndPoints.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

enum HTTPRouter {
    case search
    case movieDetails(id: Int)
    case tvShowDetails(id: Int)
    
    var url: URL {
        switch self {
        case .search:
            return HTTPClient.shared.endpoint.appendingPathComponent("search/multi")
            
        case .movieDetails(let id):
            return HTTPClient.shared.endpoint.appendingPathComponent("movie/\(id)")
            
        case .tvShowDetails(let id):
            return HTTPClient.shared.endpoint.appendingPathComponent("tv/\(id)")
        }
    }
}
