//
//  VideoType.swift
//  Movies
//
//  Created by kostis stefanou on 7/20/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc enum VideoType: Int {
    case trailer = 0
    case teaser = 1
    case clip = 2
    case featurette = 3
    case behindTheScenes = 4
    case bloopers = 5
    case other = 6
    
    init(stringType: String?) {
        switch stringType {
        case "Trailer": self.init(rawValue: 0)!
        case "Teaser": self.init(rawValue: 1)!
        case "Clip": self.init(rawValue: 2)!
        case "Featurette": self.init(rawValue: 3)!
        case "Behind the scenes": self.init(rawValue: 4)!
        case "Bloopers": self.init(rawValue: 5)!
        default: self.init(rawValue: 6)!
        }
    }
}
