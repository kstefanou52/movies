//
//  VideoProvider.swift
//  Movies
//
//  Created by kostis stefanou on 7/20/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc enum VideoProvider: Int {
    case youtube = 0
    case vimeo = 1
    case other = 3
    
    init(stringType: String?) {
        switch stringType {
        case "YouTube": self.init(rawValue: 0)!
        case "Vimeo": self.init(rawValue: 1)!
        default: self.init(rawValue: 3)!
        }
    }
    
    func url(with key: String?) -> String {
        guard let key = key else { return "" }
        switch self {
        case .youtube: return "https://www.youtube.com/watch?v=\(key)"
        case .vimeo: return "https://vimeo.com/\(key)"
        case .other: return ""
        }
    }
}
