//
//  MediaType.swift
//  Movies
//
//  Created by kostis stefanou on 7/18/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc enum MediaType: Int {
    case movie = 0
    case tvShow = 1
    
    init?(stringType: String?) {
        switch stringType {
        case "movie": self.init(rawValue: 0)
        case "tv": self.init(rawValue: 1)
        default: return nil
        }
    }
}
