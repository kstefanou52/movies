//
//  HTTPClient.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc class HTTPClient: NSObject {
    
    @objc static let shared = HTTPClient()
    private let session = URLSession(configuration: .default)
    
    let endpoint: URL = URL(string: "https://api.themoviedb.org/3/")!
    private let apiKey: String = "6b2e856adafcc7be98bdf0d8b076851c"
    
    private func getURLRequest(endPoint: URL, params: [String: Any] = [:]) -> URLRequest? {
        var parametersWithAPIKey = params
        parametersWithAPIKey["api_key"] = apiKey
        
        var request = URLRequest(url: endPoint.appendingQueryParameters(parametersWithAPIKey))
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        return request
    }
    
    private func performURLDataTask<T: Codable>(with request: URLRequest?, completion: @escaping(T?, HTTPClientError) -> Void) {
        guard let request = request else { completion(nil, .invalidRequest) ; return }
        
        session.dataTask(with: request) { (data, urlResponse, error) in
            guard let statusCode = (urlResponse as? HTTPURLResponse)?.statusCode else { completion(nil, .invalidResponse) ; return }
            guard statusCode == 200 else {
                let decodedErrorData = try? JSONDecoder().decode(APIResponseError.self, from: data ?? Data())
                print(decodedErrorData?.statusMessage ?? "")
                
                let httpClientError = HTTPClientError(rawValue: statusCode) ?? .invalidRequest
                completion(nil, httpClientError) ; return
            }
            
            guard let data = data, error == nil else { completion(nil, .invalidResponse) ; return }
            
            do {
                let decodedData = try JSONDecoder().decode(T.self, from: data)
                completion(decodedData, .none)
            } catch {
                print(error)
                completion(nil, .parsingError)
            }
        }.resume()
    }
    
    private func performURLDataTask(with url: URL, completion: @escaping(Data?) -> Void) {
        session.dataTask(with: url) { (data, response, error) in
            guard let data = data, error == nil else { print(error.debugDescription) ; completion(nil) ; return }
            completion(data)
        }.resume()
    }
}

// MARK: API
extension HTTPClient {
    func getSearchResults(with query: String, page: Int, completion: @escaping(APIResponseSearch?, HTTPClientError) -> Void) {
        let parameters: [String: Any] = ["query" : query, "page" : page]
        let request = getURLRequest(endPoint: HTTPRouter.search.url, params: parameters)
        
        performURLDataTask(with: request, completion: completion)
    }
    
    @objc func getMovieDetails(for movieId: Int, completion: @escaping(APIResponseMovieDetails?, HTTPClientError) -> Void) {
        let parameters = ["append_to_response" : "videos"]
        let request = getURLRequest(endPoint: HTTPRouter.movieDetails(id: movieId).url, params: parameters)
        performURLDataTask(with: request, completion: completion)
    }
    
    @objc func getTVShowDetails(for tvShowId: Int, completion: @escaping(APIResponseTVShowDetails?, HTTPClientError) -> Void) {
        let parameters = ["append_to_response" : "videos"]
        let request = getURLRequest(endPoint: HTTPRouter.tvShowDetails(id: tvShowId).url, params: parameters)
        performURLDataTask(with: request, completion: completion)
    }
    
    @objc func getImage(path: String?, completion: @escaping(Data?) -> Void) {
        guard let path = path else { completion(nil) ; return }
        guard let imageURL = URL(string: "https://image.tmdb.org/t/p/w300\(path)") else { completion(nil) ; return }
        performURLDataTask(with: imageURL, completion: completion)
    }
}
