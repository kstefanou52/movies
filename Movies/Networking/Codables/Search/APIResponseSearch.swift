//
//  APIResponseSearch.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

struct APIResponseSearch: Codable {
    var results: [APIResponseSearchResult]? = []
    var page: Int?
    var totalPages: Int?
    
    enum CodingKeys: String, CodingKey {
        case results = "results"
        case page = "page"
        case totalPages = "total_pages"
    }
}

struct APIResponseSearchResult: Codable {
    var backdropPath: String?
    var id: Int?
    var originalName: String?
    var popularity: Double?
    var firstAirDate: String?
    var releaseDate: String?
    var overview: String?
    var voteAverage: Double?
    var originalLanguage: String?
    var posterPath: String?
    var mediaTypeText: String?
    var originCountry: [String]? = []
    var voteCount: Int?
    var name: String?
    
    //custom variabes
    var mediaType: MediaType? {
        return MediaType.init(stringType: mediaTypeText)
    }
    
    enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case id = "id"
        case originalName = "original_name"
        case popularity = "popularity"
        case firstAirDate = "first_air_date"
        case releaseDate = "release_date"
        case overview = "overview"
        case voteAverage = "vote_average"
        case originalLanguage = "original_language"
        case posterPath = "poster_path"
        case mediaTypeText = "media_type"
        case originCountry = "origin_country"
        case voteCount = "vote_count"
        case name = "name"
    }
}
