//
//  APIResponseMovieVideo.swift
//  Movies
//
//  Created by kostis stefanou on 7/20/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc class APIResponseVideo: NSObject, Codable {
    @objc let results: [APIResponseVideoResult]?
    
    enum CodingKeys: String, CodingKey {
        case results = "results"
    }
}

@objc class APIResponseVideoResult: NSObject, Codable {
    @objc let key: String?
    @objc let site: String?
    let size: Int?
    @objc let name: String?
    let id: String?
    let type: String?
    
    //custom variables
    @objc var videoProvider: VideoProvider {
        return VideoProvider.init(stringType: site)
    }
    
    @objc var videoType: VideoType {
        return VideoType.init(stringType: type)
    }
    
    @objc var videoURL: String {
        return videoProvider.url(with: key)
    }
    
    enum CodingKeys: String, CodingKey {
        case key = "key"
        case site = "site"
        case size = "size"
        case name = "name"
        case id = "id"
        case type = "type"
    }
}
