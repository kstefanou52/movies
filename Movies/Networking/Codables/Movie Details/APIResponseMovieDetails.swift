//
//  APIResponseMovieDetails.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc class APIResponseMovieDetails: NSObject, Codable {
    let homepage: String?
    @objc let overview: String?
    let popularity: Double?
    let originalTitle: String?
    let id: Int?
    @objc let video: Bool
    let imdbId: String?
    let releaseDate: String?
    @objc let title: String?
    let posterPath: String?
    let voteAverage: Double?
    let runtime: Int?
    let originalLanguage: String?
    let status: String?
    let adult: Bool?
    @objc let genres: [APIResponseMovieDetailsGenre] = []
    let voteCount: Int?
    let revenue: Int?
    @objc let backdropPath: String?
    let tagline: String?
    let budget: Int?
    @objc let videos: APIResponseVideo?
    
    enum CodingKeys: String, CodingKey {
        case homepage = "homepage"
        case overview = "overview"
        case popularity = "popularity"
        case originalTitle = "original_title"
        case id = "id"
        case video = "video"
        case imdbId = "imdb_id"
        case releaseDate = "release_date"
        case title = "title"
        case posterPath = "poster_path"
        case voteAverage = "vote_average"
        case runtime = "runtime"
        case originalLanguage = "original_language"
        case status = "status"
        case adult = "adult"
        case genres = "genres"
        case voteCount = "vote_count"
        case revenue = "revenue"
        case backdropPath = "backdrop_path"
        case tagline = "tagline"
        case budget = "budget"
        case videos = "videos"
    }
}

@objc class APIResponseMovieDetailsGenre: NSObject, Codable {
    let id: Int?
    @objc let name: String?
}
