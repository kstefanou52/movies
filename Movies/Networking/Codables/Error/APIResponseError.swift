//
//  APIResponseError.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

struct APIResponseError: Codable {
    var statusMessage: String?
    var success: Bool?
    var statusCode: Int?
    
    enum CodingKeys: String, CodingKey {
        case statusMessage = "status_message"
        case success = "success"
        case statusCode = "status_code"
    }
}
