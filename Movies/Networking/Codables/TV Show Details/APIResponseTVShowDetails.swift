//
//  APIResponseTVShowDetails.swift
//  Movies
//
//  Created by kostis stefanou on 7/17/19.
//  Copyright © 2019 silonk. All rights reserved.
//

import Foundation

@objc class APIResponseTVShowDetails: NSObject, Codable {
    let lastAirDate: String?
    let homepage: String?
    @objc let overview: String?
    @objc let name: String?
    let lastEpisodeToAir: APIResponseTVShowDetailsLastEpisodeToAir?
    let popularity: Double?
    let id: Int?
    let numberOfEpisodes: Int?
    let originalName: String?
    let createdBy: [APIResponseTVShowDetailsCreatedBy] = []
    let numberOfSeasons: Int?
    let posterPath: String?
    let languages: [String?] = []
    let voteAverage: Double?
    let originalLanguage: String?
    let networks: [APIResponseTVShowDetailsNetwork] = []
    let seasons: [APIResponseTVShowDetailsSeason] = []
    let episodeRunTime: [Int?] = []
    let firstAirDate: String?
    let status: String?
    let type: String?
    @objc let genres: [APIResponseTVShowDetailsGenre] = []
    let voteCount: Int?
    let productionCompanies: [APIResponseTVShowDetailsProductionCompanie] = []
    let originCountry: [String?] = []
    @objc let backdropPath: String?
    let inProduction: Bool?
    @objc let videos: APIResponseVideo?
    
    enum CodingKeys: String, CodingKey {
        case lastAirDate = "last_air_date"
        case homepage = "homepage"
        case overview = "overview"
        case name = "name"
        case lastEpisodeToAir = "last_episode_to_air"
        case popularity = "popularity"
        case id = "id"
        case numberOfEpisodes = "number_of_episodes"
        case originalName = "original_name"
        case createdBy = "created_by"
        case numberOfSeasons = "number_of_seasons"
        case posterPath = "poster_path"
        case languages = "languages"
        case voteAverage = "vote_average"
        case originalLanguage = "original_language"
        case networks = "networks"
        case seasons = "seasons"
        case episodeRunTime = "episode_run_time"
        case firstAirDate = "first_air_date"
        case status = "status"
        case type = "type"
        case genres = "genres"
        case voteCount = "vote_count"
        case productionCompanies = "production_companies"
        case originCountry = "origin_country"
        case backdropPath = "backdrop_path"
        case inProduction = "in_production"
        case videos = "videos"
    }
}

struct APIResponseTVShowDetailsProductionCompanie: Codable {
    let originCountry: String?
    let logoPath: String?
    let id: Int?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case originCountry = "origin_country"
        case logoPath = "logo_path"
        case id = "id"
        case name = "name"
    }
}

struct APIResponseTVShowDetailsLastEpisodeToAir: Codable {
    let seasonNumber: Int?
    let id: Int?
    let showId: Int?
    let stillPath: String?
    let airDate: String?
    let voteCount: Int?
    let overview: String?
    let name: String?
    let episodeNumber: Int?
    let voteAverage: Double?
    let productionCode: String?
    
    enum CodingKeys: String, CodingKey {
        case seasonNumber = "season_number"
        case id = "id"
        case showId = "show_id"
        case stillPath = "still_path"
        case airDate = "air_date"
        case voteCount = "vote_count"
        case overview = "overview"
        case name = "name"
        case episodeNumber = "episode_number"
        case voteAverage = "vote_average"
        case productionCode = "production_code"
    }
}

struct APIResponseTVShowDetailsSeason: Codable {
    let seasonNumber: Int?
    let id: Int?
    let airDate: String?
    let episodeCount: Int?
    let name: String?
    let overview: String?
    let posterPath: String?
    
    enum CodingKeys: String, CodingKey {
        case seasonNumber = "season_number"
        case id = "id"
        case airDate = "air_date"
        case episodeCount = "episode_count"
        case name = "name"
        case overview = "overview"
        case posterPath = "poster_path"
    }
}

struct APIResponseTVShowDetailsCreatedBy: Codable {
    let creditId: String?
    let profilePath: String?
    let id: Int?
    let gender: Int?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case creditId = "credit_id"
        case profilePath = "profile_path"
        case id = "id"
        case gender = "gender"
        case name = "name"
    }
}

struct APIResponseTVShowDetailsNetwork: Codable {
    let originCountry: String?
    let logoPath: String?
    let id: Int?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case originCountry = "origin_country"
        case logoPath = "logo_path"
        case id = "id"
        case name = "name"
    }
}

@objc class APIResponseTVShowDetailsGenre: NSObject, Codable {
    let id: Int?
    @objc let name: String?
}
